package uk.ac.cam.psychometrics.papi.client.jersey;

interface Resource {

    boolean isConfigured();

}
