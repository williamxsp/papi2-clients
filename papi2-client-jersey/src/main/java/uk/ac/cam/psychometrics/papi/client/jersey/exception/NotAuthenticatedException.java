package uk.ac.cam.psychometrics.papi.client.jersey.exception;

public class NotAuthenticatedException extends PapiClientException {

    public NotAuthenticatedException(final String message) {
        super(message);
    }

}
