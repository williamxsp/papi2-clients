package uk.ac.cam.psychometrics.papi.client.spring;

import org.springframework.web.client.RestTemplate;

public class PapiClient {

    private static final String ERROR_RESOURCE_CONFIG = "The resource is not configured";

    public static final class Config {
        private RestTemplate restTemplate;
        private String serviceUrl;

        public Config() {
        }

        public Config withHttpEndpoint(RestTemplate restTemplate, String serviceUrl) {
            this.restTemplate = restTemplate;
            this.serviceUrl = serviceUrl;
            return this;
        }

    }

    private final Resource authResource;
    private final Resource predictionResource;

    public PapiClient(Config config) {
        authResource = new AuthResourceImpl(config.restTemplate, config.serviceUrl + "/auth");
        predictionResource = new PredictionResourceImpl(config.restTemplate, config.serviceUrl);
    }

    public AuthResource getAuthResource() {
        if (authResource.isConfigured()) {
            return (AuthResource) authResource;
        } else {
            throw new IllegalStateException(ERROR_RESOURCE_CONFIG);
        }
    }

    public PredictionResource getPredictionResource() {
        if (predictionResource.isConfigured()) {
            return (PredictionResource) predictionResource;
        } else {
            throw new IllegalStateException(ERROR_RESOURCE_CONFIG);
        }
    }

}
