package uk.ac.cam.psychometrics.papi.client.spring.exception;

public class PapiClientException extends RuntimeException {

    public PapiClientException(String message) {
        super(message);
    }

    public PapiClientException(final String message, final Throwable cause) {
        super(message, cause);
    }

}
