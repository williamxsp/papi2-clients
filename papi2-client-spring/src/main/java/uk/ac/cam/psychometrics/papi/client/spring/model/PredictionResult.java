package uk.ac.cam.psychometrics.papi.client.spring.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Objects;
import java.util.Set;

public class PredictionResult implements Serializable {

    private final String uid;
    private final Set<Prediction> predictions;

    @JsonCreator
    public PredictionResult(@JsonProperty("uid") final String uid,
                            @JsonProperty("predictions") final Set<Prediction> predictions) {
        this.uid = uid;
        this.predictions = predictions;
    }

    public Prediction getPrediction(Trait trait) {
        for (Prediction prediction : predictions) {
            if (prediction.getTrait().equals(trait)) {
                return prediction;
            }
        }
        return null;
    }

    public String getUid() {
        return uid;
    }

    public Set<Prediction> getPredictions() {
        return predictions;
    }

    @Override
    public int hashCode() {
        return Objects.hash(uid, predictions);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final PredictionResult other = (PredictionResult) obj;
        return Objects.equals(this.uid, other.uid) && Objects.equals(this.predictions, other.predictions);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("PredictionResult{");
        sb.append("uid=").append(uid);
        sb.append(", predictions=").append(predictions);
        sb.append('}');
        return sb.toString();
    }

}
